TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    board.cpp \
    player.cpp \
    game.cpp \
    ship.cpp

HEADERS += \
    board.h \
    player.h \
    game.h \
    ship.h
