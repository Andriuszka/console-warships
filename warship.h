#ifndef WARSHIP_H
#define WARSHIP_H
#include <iostream>

using namespace std;

class Warship
{
    int *shipPosistionY;
    int *shipPositionX;
    int shipSize;
    char shipSymbol;

public:
    Warship(int sizeValue);
    ~Warship();

    void setShipPosition(int moduleNumber, int posY, int posX);
};

#endif // WARSHIP_H
