#include "board.h"

Board::Board()
{
    initializeBoard();
}

Board::~Board()
{
    for( int i = 0; i < BOARDSIZE; i++)
    {
        delete[] board[ i ];
    }
    delete[] board;
}

void Board::initializeBoard()
{
    //for now I need to start incrementing first column
    // and easiest way is to initiate it by simple char
    char firstLetter = 'A';

    // Allocating memory for 2-dim array (our board)
    board = new char* [ BOARDSIZE ];
    for( int i = 0; i < BOARDSIZE; i++ )
    {
        board[ i ] = new char[ BOARDSIZE ];
    }

    // Filling first column of the board with letters
    for( int line = 0; line < BOARDSIZE; line++)
    {
        int column = 0;
        board[ line ][ column ] = firstLetter;
        firstLetter++;
    }

    // Filling the rest of the board with '~'
    for( int line = 0; line < BOARDSIZE; line++ )
    {
        for( int column = 1; column < BOARDSIZE; column++ )
        {
            board[ line ][ column ] = '~';
        }
    }
}

void Board::clearBoard()
{
    char firstLetter = 'A';
    // Filling first column of the board with letters
    for( int line = 0; line < BOARDSIZE; line++)
    {
        int column = 0;
        board[ line ][ column ] = firstLetter;
        firstLetter++;
    }

    // Filling the rest of the board with '~'
    for( int line = 0; line < BOARDSIZE; line++ )
    {
        for( int column = 1; column < BOARDSIZE; column++ )
        {
            board[ line ][ column ] = '~';
        }
    }
}
void Board::displayBoard()
{
    // "gimmic" row, that will be indicating numbers,
    // as point of reference for player
    for( int i = 0; i < BOARDSIZE; i++)
    {
        cout << i << " ";
    }
    cout << endl;

    for( int line = 0; line < BOARDSIZE; ++line )
    {
        for( int column = 0; column < BOARDSIZE; ++column)
        {
            cout << board[ line ][ column ] << " ";
        }
        cout<< endl;
    }
    cout << endl;
}

void Board::insertShipsPositions( int valueY, int valueX )
{
    board[ valueY ][ valueX ] = 'O';
}

void Board::insertPlayerPosition( int valueY, int valueX )
{
    board[ valueY ][ valueX ] = '?';
}

void Board::clearPlayerPosition( int valueY, int valueX )
{
    board[ valueY ][ valueX ] = '~';
}

void Board::insert_misses( int valueY, int valueX )
{
    board[ valueY ][ valueX ] = 'X';
}

void Board::insert_shots_on_target( int valueY, int valueX )
{
    board[ valueY ][ valueX ] = '*';
}



