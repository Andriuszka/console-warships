#ifndef GAME_H
#define GAME_H
#include <iostream>
#include <board.h>
#include <player.h>

using namespace std;



class Game
{



    char action;

    // Flags that trigger parts of the game

    //!
    //! \brief gameOn
    //! Main trigger for the game
    bool gameOn;

    //!
    //! \brief players_ready
    //! Used to check if players are ready for second part of the game (shooting part)
    bool players_ready;

    //!
    //! \brief shoot_made
    //! Flag used in second part of the gameplay ( shooting ). Players are switched after a shot is made.
    bool shoot_made;

    // Elements of the gameplay

    //!
    //! \brief player_counter
    //! Used with adding players
    int player_counter;
    const int player_amount;

    Board* player_board;
    Board* shooting_board;
    Player* current_player;
    Player* opposing_player;
    Player* player_arr;

public:
    Game();
    ~Game();

// Mechanics for adding and switching players during game.

    //!
    //! \brief addPlayer
    //! Adding players and checking how many of them there is.
    //! \param a_pNewPlayer
    //! Pointer recieved in the main to add players to the game
    void addPlayer(Player* a_pNewPlayer);

    //!
    //! \brief switchPlayer
    //! Switching players.
    //! In first part of gameplay its used after players
    //! set their ships.
    //! In secon phase of game it's called when a shot is made
    void switchPlayer();


// Interface, game window and graphics content

    //!
    //! \brief battleship_display
    //! A little graphics touch. Displays an ascii ship at the top of the frame ;)
    void battleship_display();

    //!
    //! \brief showWhichPlayer
    //! Displays information about which players is the current one
    void showWhichPlayer();

    //!
    //! \brief initiateShips
    //! Inserts ships of the current player to the displayed board.
    void initiateShips();

    //!
    //! \brief insert_hits
    //! Inserts hit marks ( on target ) to the displayed board.
    void insert_hits();

    //!
    //! \brief insert_missfires
    //! Inserts missed hit marks to the displayed board.
    void insert_missfires();

    //!
    //! \brief gameInterface
    //! Displays game main instructions accordingly to game phase.
    //! It also carries out and calls shooting mechanics, when it's the secon phase of the game.
    //! Also here, game checks if any player won. ( calls player->check if dead )
    void gameInterface();


// All game mechanics functions

    //!
    //! \brief gamePlay
    //! Game loop for the first part of the game. Carries out logic for
    //! displaying and movement/action of the player. It ends when all ships are set.
    void gamePlay();

    //!
    //! \brief getAction
    //! Getter function used in Player class.
    //! \return
    //! Passes char value used for switching actions in Player class
    char getAction();

    //!
    //! \brief check_if_ready
    //!  Function checks if all the ships are set, and it's time for secon phase of the game
    //! \return
    //! Boolean value used as flag for initiating second phase of the game mechanics
    bool check_if_ready();

    //!
    //! \brief gamePlayPhaseTwo
    //! Game loop for the secon part of the game. Like gamePLay(), carries out logic
    //! for displaying and movement/firing of the player. It ends when one of the players
    //! recieves the same amount of damage as his Health Points ( all ships are sunk)
    void gamePlayPhaseTwo();

    //!
    //! \brief Player_shoots
    //! Shooting trigger, carried out by Player class. Passes position of the shot and
    //! gives feedback about hit or miss.
    void Player_shoots();
};

#endif // GAME_H
