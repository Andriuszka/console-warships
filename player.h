#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <ship.h>
#define DEFAULT_PLAYER_POSITION 3
#define BOARDSIZE 10
#define SHIPS_AMOUNT 10
#define SHOTS_AMOUNT 100
#define PLAYER_HP 20

using namespace std;

class Player
{

// Main class members, containing info for both parts of the game
//////////////////////////////////////////////////////////////////////////////

    int positionX;
    int positionY;

    //!
    //! \brief ship_count
    //! This parameter counts a number of ships already marked on the board by player.
    int ship_count;
    //!
    //! \brief PlayerShips
    //! Array of ship objects assigned to player
    ship* PlayerShips;

// Class members tied to second phase of the game
//////////////////////////////////////////////////////////////////////////////

    int player_damage;
    int* hits_takenY;
    int* hits_takenX;

    int* missfires_takenY;
    int* missfires_takenX;
    int missfires_taken_counter;

    int* player_shotsY;
    int* player_shotsX;

public:
    Player();
    ~Player();

// General player info, and moving functions
//////////////////////////////////////////////////////////////////////////////

    void showplayerInfo1();

    //!
    //! \brief playerMove
    //! ovely complicated function, that handles action - understood as change of player position and ship marking on the board
    //! It is being used in Game::gameplay()
    //! \param action
    //! char parameter value passed from game class
    void playerMove( char action );

    //!
    //! \brief getPositionX
    //! \return
    //! int value of player X position
    int getPositionX();

    //!
    //! \brief getPositionY
    //! \return
    //! int value of player Y position
    int getPositionY();

// Functions for setting and unsetting ships
///////////////////////////////////////////////////////////////////////

    void setWarhip( int setY, int setX );
    void cancel_ship_position();
    bool finished_setting_ships();


// Functions returning information about existing ships
//////////////////////////////////////////////////////////////////////
    int getShipY( int count, int element );
    int getShipX( int count, int element );

    //!
    //! \brief getShipCount
    //! Funtion used in Game::initiateShips(). It gives an amount of ALREADY MARKED ships
    //! \return
    //! int value of ships that have been marked on the board
    int getShipCount();

    //!
    //! \brief getShipSize
    //! Function used in Game::initiateShips(). It refers to an array of ships for current player and gives size of specific ship object.
    //! \param shipNumber
    //! int value representing number of a desired ship.
    //! \return
    //! int value of the ship
    int getShipSize(int shipNumber);

// Functions as. with shooting, taking damage and checking player's HP.
///////////////////////////////////////////////////////////////////////

    int get_player_damage();

    int get_hits_takenY( int counter );

    int get_hits_takenX( int counter );

    int get_missfires_taken_counter();

    int get_missfires_takenY( int counter );

    int get_missfires_takenX( int counter );

    bool take_hit( int Y, int X );

    bool check_if_dead();
};

#endif // PLAYER_H
