#include "ship.h"

ship::ship( int size ):
    place_counter( 0 )
{
    ship_Size = size;
    shipY = new int[ size ];
    shipX = new int[ size ];

    for(int i = 0; i < size; i++)
    {
        shipY[ i ] = INITIAL_POSITION;
        shipX[ i ] = INITIAL_POSITION;
    }
}

ship::~ship()
{
    delete[] shipY;
    delete[] shipX;
}

int ship::getShip_Size()
{
    return ship_Size;
}

int ship::get_place_counter()
{
    return place_counter;
}

void ship::reset_place_counter()
{
    place_counter = 0;
}

int ship::getShipY( int element )
{
    return shipY[ element ];
}

int ship::getShipX( int element )
{
    return shipX[ element ];
}

bool ship::check_ship_integrity( int setY, int setX )
{
    if( ship_Size > 1 && place_counter > 0 )
    {
        if( abs( setY - shipY[ ( place_counter - 1 ) ] ) > 1 )
        {
            return true;
        }
        else if( abs( setX - shipX[ (place_counter - 1) ] ) > 1 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

void ship::setShipElement( int setY, int setX )
{
    if( place_counter < ship_Size )
    {
        shipY[ place_counter ] = setY;
        shipX[ place_counter ] = setX;
        place_counter++;
    }
    else
    {
        cout << endl << " Statek osiagnal maksymalna liczbe elementow" << endl;
        cout << " Cos poszlo grubo nie tak" << endl;
        system("pause");
    }
}

void ship::clear_ship()
{
    for( int i = 0; i < ship_Size; i++ )
    {
        shipY[ i ] = INITIAL_POSITION;
        shipX[ i ] = INITIAL_POSITION;
    }
}

void ship::eliminate_ship_element( int counter )
{
    shipY[ counter ] = 0;
    shipX[ counter ] = 0;
}





