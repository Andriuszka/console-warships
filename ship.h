#ifndef SHIP_H
#define SHIP_H
#include <iostream>
#include <cmath>

// initial "position of ships - just outside of board.
// that clears positions numbered as '0' - initial value of int.
#define INITIAL_POSITION 11

using namespace std;

class ship
{
    int ship_Size;

    //!
    //! \brief place_counter
    //! This parameter is a kind of iterator that increments in order to
    //! mark elements of the ship object. A simple way to check if all elements
    //! of the ship have been correctly marked, and are not left unitiated.
    int place_counter;
    int *shipY;
    int *shipX;

public:

    //!
    //! \brief ship
    //! Default constructor for ship objects for all players. Its called in
    //! Player class, where correct sizes are being specified
    //! \param size
    //! int parameter passed with creation of ships array in player class
    ship( int size );
    ~ship();

    int getShip_Size();

    //!
    //! \brief get_place_counter
    //! \return
    //! returns int value representing number of the element that will be marked
    int get_place_counter();
    void reset_place_counter();

    int getShipY( int element );
    int getShipX( int element );

    //!
    //! \brief check_ship_integrity
    //! Checks if desired position of new ship element to set is next to previous element.
    //! \param setY
    //! int param, representig Y position to check
    //! \param setX
    //! int param, representig X position to check
    //! \return
    //! Bool value of the error check
    bool check_ship_integrity( int setY, int setX);

    //!
    //! \brief setShipElement
    //! Assigns position of the specified element with parameters passed to it.
    //! \param setY
    //! \param setX
    void setShipElement( int setY, int setX );

    //!
    //! \brief clear_ship
    //! Clears all elements of the ship from previous data and replaces it with 0 val.
    void clear_ship();

    //!
    //! \brief eliminate_ship_element
    //! Replaces specified element of the ship with INITIAL_POSITION value.
    //! \param counter
    //! Int parameter representing number of the element to clear.
    void eliminate_ship_element( int counter );
};

#endif // SHIP_H
