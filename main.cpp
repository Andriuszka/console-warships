#include <iostream>
#include "board.h"
#include "player.h"
#include "game.h"

using namespace std;

int main()
{
    Game *game = new Game;
    Player *player1 = new Player;
    Player *player2 = new Player;

    game->addPlayer( player1 );
    game->addPlayer( player2 );
    game->gamePlay();
    game->gamePlayPhaseTwo();

    delete game;
    delete player1;
    delete player2;


    return 0;
}
