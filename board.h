#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#define BOARDSIZE 10

using namespace std;

class Board
{
    char **board;
public:
    Board();
    ~Board();

    //!
    //! \brief initializeBoard
    //! Function used in constructor. It allocates two dimentional char array and fills
    //! it with preliminary values.
    //! ---------------------------
    void initializeBoard();

    //!
    //! \brief clearBoard
    //! Function used in Game::gameplay() function. It clears the board so it can be filled with fresh positions from player.
    void clearBoard();

    //!
    //! \brief displayBoard
    //! Function displays the current state of board.
    //! ----------------------------------------------
    void displayBoard();

    //!
    //! \brief insertShipsPositions
    //! Funtion used in game.cpp-> initiateShips(), used to insert ships into board
    //! \param valueY
    //! int parameter passed from member pointer to current player. Gives Y position of a current element and current ship
    //! \param valueX
    //! int parameter passed from member pointer to current player. Gives X position of a current element and current ship
    void insertShipsPositions( int valueY, int valueX );

    //!
    //! \brief insertPlayer
    //! funtion inserts a new position of the player to be later displayed
    //! \param valueY
    //!  int parameter passed by movement function. Represents line on the board
    //! \param valueX
    //!  int parameter passed by movement function. Represents line on the board
    //! ----------------------------------------------------------------------
    void insertPlayerPosition( int valueY, int valueX );

    //!
    //! \brief clearPlayerPosition
    //! Funtion clears current player position from board and replaces it
    //! with default 'x' symbol on the board
    //! \param valueY
    //!  int parameter passed by movement function. Represents line on the board
    //! \param valueX
    //!  int parameter passed by movement function. Represents line on the board
    void clearPlayerPosition( int valueY, int valueX );

    //!
    //! \brief insert_misses
    //! Function used in Game class. Takes Y and X parameters from missfires array in the Player class
    //! \param valueY
    //! \param valueX
    void insert_misses( int valueY, int valueX );

    //!
    //! \brief insert_shots_on_target
    //! Function used in Game class. Takes Y and X parameters from shots on tharget in Player class.
    //! \param valueY
    //! \param valueX
    void insert_shots_on_target( int valueY, int valueX );



};

#endif // BOARD_H
