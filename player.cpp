#include "player.h"

Player::Player():
    positionX( DEFAULT_PLAYER_POSITION ),
    positionY( DEFAULT_PLAYER_POSITION ),
    ship_count( 0 ),
    PlayerShips( new ship[ SHIPS_AMOUNT ] { {1}, {1}, {1}, {1}, {2}, {2}, {2}, {3}, {3}, {4} } ),
    player_damage( 0 ),
    hits_takenY( new int [ PLAYER_HP ] ),
    hits_takenX( new int [ PLAYER_HP ] ),
    missfires_takenY( new int [ SHOTS_AMOUNT ]),
    missfires_takenX( new int [ SHOTS_AMOUNT ]),
    missfires_taken_counter( 0 ),
    player_shotsY( new int[ SHOTS_AMOUNT ]),
    player_shotsX( new int[ SHOTS_AMOUNT ])
{
}

Player::~Player()
{
    delete[] PlayerShips;
    delete[] hits_takenY;
    delete[] hits_takenX;
    delete[] missfires_takenY;
    delete[] missfires_takenX;
    delete[] player_shotsY;
    delete[] player_shotsX;
}

/////////////////////////////////////////////////////////////////////////

void Player::showplayerInfo1()
{
    cout << endl << "Oznaczono " << ship_count << " z " << SHIPS_AMOUNT << " statkow " << endl;
    if( ship_count < SHIPS_AMOUNT )
    {
        cout << "Obecnie oznaczasz statek o wielkosci: " << PlayerShips[ ship_count ].getShip_Size() << endl;
        cout << "Oznaczono " << PlayerShips[ ship_count ].get_place_counter() << " z " << PlayerShips [ ship_count ].getShip_Size() << " elementow statku " << endl;
    }
}

void Player::playerMove( char action )
{
    switch( action )
    {
    case 's':
        if( positionY + 1 == ( BOARDSIZE ) )
        {
            positionY = 0;
        }
        else
        {
            ++positionY;
        }
        break;

    case 'w':
        if(positionY - 1 < 0)
        {
            positionY = ( BOARDSIZE - 1 );
        }
        else
        {
            positionY--;
        }
        break;

    case 'd':
        if(positionX + 1 > ( BOARDSIZE - 1 ) )
        {
            positionX = 1;
        }
        else
        {
            positionX++;
        }
        break;

    case 'a':
        if(positionX - 1 == 0)
        {
            positionX = ( BOARDSIZE - 1 );
        }
        else
        {
            positionX--;
        }
        break;

    case 'f':
        {
            setWarhip( positionY, positionX );
        }
        break;

    case 'c':
        {
            cancel_ship_position();
            break;
        }

    default:
        break;
    }
}

int Player::getPositionX()
{
    return positionX;
}

int Player::getPositionY()
{
    return positionY;
}

/////////////////////////////////////////////////////////////////////////

void Player::setWarhip( int setY, int setX )
{
    // Overly complicated error check ;)
    bool badPlace = false;

    badPlace = PlayerShips[ ship_count ].check_ship_integrity( setY, setX );

    for( int i = 0; i < (ship_count+1); i++ )
    {
        for( int y = 0; y < PlayerShips [ i ].getShip_Size(); y++ )
        {
            if( ( PlayerShips[ i ].getShipY( y ) ) == setY )
            {
                for( int x = 0; x < PlayerShips[ i ].getShip_Size() ; x++ )
                {
                    if( PlayerShips [ i ].getShipX( x ) == setX )
                    {
                        badPlace = true;
                        break;
                    }
                }
                break;
            }
        }
    }

    // This part checks if amount of ships is not exceeded and if desired place for ship placement isn't already taken
    if( ship_count < SHIPS_AMOUNT && badPlace == false )
    {
        PlayerShips[ ship_count ].setShipElement( setY, setX );

        // Now we need to check if all of current's ship elements are already marked. if so...
        if( PlayerShips[ ship_count ].get_place_counter() < PlayerShips[ ship_count ].getShip_Size())
        {
            cout << endl << " Oznaczono kolejny element" << endl;
            system( "pause" );
        }
        else
        {
            // ... ship count is incremented, and next time we will operate on another ship.
            cout << endl << "Oznaczono statek nr: " << ship_count + 1 << endl;
            ship_count++;
            system( "pause" );
        }

    }
    else
    {
        cout << endl << "Osiagnieto maksymalna liczbe statkow lub statek umieszczono w zlym miejscu" << endl;
        cout << "Pamietaj, ze statki zwykle nie maja dziur ;)" << endl;
        system( "pause" );
    }
}

void Player::cancel_ship_position()
{

    if( PlayerShips[ ship_count ].get_place_counter() == 0 && ship_count > 0)
    {
        ship_count--;
        PlayerShips[ ship_count ].clear_ship();
        PlayerShips[ ship_count ].reset_place_counter();
        cout << endl << "Cofnieto oznaczenie statku" << endl;
        system( "pause" );
    }
    else
    {
        PlayerShips[ ship_count ].clear_ship();
        PlayerShips[ ship_count ].reset_place_counter();
        cout << endl << "Cofnieto oznaczenie statku" << endl;
        system( "pause" );
    }

}

bool Player::finished_setting_ships()
{
    if( ship_count == SHIPS_AMOUNT)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/////////////////////////////////////////////////////////////////////////

int Player::getShipY( int count, int element )
{
    return PlayerShips[ count ].getShipY( element );
}

int Player::getShipX( int count, int element )
{
    return PlayerShips[ count ].getShipX( element );
}

int Player::getShipCount()
{
    return ship_count;
}

int Player::getShipSize( int shipNumber )
{
    return PlayerShips[ shipNumber ].getShip_Size();
}

//////////////////////////////////////////////////////////////////////////

int Player::get_player_damage()
{
    return player_damage;
}

int Player::get_hits_takenY( int counter )
{
    return hits_takenY[ counter ];
}

int Player::get_hits_takenX( int counter )
{
    return hits_takenX[ counter ];
}

int Player::get_missfires_taken_counter()
{
    return missfires_taken_counter;
}

int Player::get_missfires_takenY( int counter )
{
    return missfires_takenY[ counter ];
}

int Player::get_missfires_takenX( int counter )
{
    return missfires_takenX[ counter ];
}

bool Player::take_hit( int Y, int X )
{

    bool hit_taken = false;

    //Checking if hit is succesful

    for( int i = 0; i < ship_count; i++)
    {
        for( int y = 0; y < PlayerShips[ i ].getShip_Size(); y++)
        {
            if( ( PlayerShips[ i ].getShipY( y ) ) == Y )
            {
                for( int x = 0; x < PlayerShips[ i ].getShip_Size() ; x++ )
                {
                    if( PlayerShips [ i ].getShipX( x ) == X )
                    {
                        PlayerShips[ i ].eliminate_ship_element( x );
                        hit_taken = true;
                        break;
                    }
                }
                break;
            }
        }
    }

    // Making changes if hit is succesful

    if(hit_taken)
    {
        hits_takenY[ player_damage ] = Y;
        hits_takenX[ player_damage ] = X;
        player_damage++;
        return true;
    }
    else
    {
        missfires_takenY[ missfires_taken_counter ] = Y;
        missfires_takenX[ missfires_taken_counter ] = X;
        missfires_taken_counter++;
        return false;
    }
}

bool Player::check_if_dead()
{
    if( player_damage == PLAYER_HP)
    {
        cout << endl << " Gratulacje! Udalo Ci sie pokonac przeciwnika! " << endl;
        cout << " KONIEC GRY " << endl;
        system("pause");
        return true;
    }
    else
    {
        return false;
    }
}

