#include "game.h"

Game::Game():
    action( 'f' ),
    gameOn( true ),
    players_ready( false ),
    shoot_made( false ),
    player_counter( 0 ),
    player_amount( 2 ),
    player_board( new Board ),
    shooting_board( new Board ),
    player_arr( new Player[ player_amount ] )
{
}

Game::~Game()
{

    delete[] player_arr;
    delete player_board;
    delete shooting_board;
}

////////////////////////////////////////////////////////

void Game::addPlayer(Player *a_pNewPlayer)
{
    if( player_counter < 2 )
    {
        a_pNewPlayer = &player_arr[ player_counter ];
        player_counter++;
        current_player = &player_arr[ 0 ];
    }
    else
    {
        cout << endl << "Osiagnieto maksymalna ilosc graczy" << endl;
        system( "pause" );
    }
}

void Game::switchPlayer()
{
    cout << endl << "Pora na zmiane gracza" << endl;
    if( current_player == &player_arr[ 1 ] )
    {
        current_player = &player_arr[ 0 ];
        opposing_player = &player_arr[ 1 ];
    }
    else
    {
       current_player = &player_arr[ 1 ];
       opposing_player = &player_arr[ 0 ];
    }
    system( "pause" );
}

////////////////////////////////////////////////////////

void Game::battleship_display()
{
cout <<"                                       # #  ( )"<<endl;
cout <<"                                    ___#_#___|__"<<endl;
cout <<"                                 _  |____________|  _"<<endl;
cout <<"                          _=====| | |            | | |==== _"<<endl;
cout <<"                    =====| |.---------------------------. | |===="<<endl;
cout <<"      <---------------------'   .  .  .  .  .  .  .  .   '--------------/"<<endl;
cout <<"        \\                                                             /"<<endl;
cout <<"         \\________________BATTLESHIPS BY SILLY_SLAV__________________/"<<endl;
cout <<"     wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"<<endl;
cout <<" wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww"<<endl;
cout <<"     wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww "<<endl;
}

void Game::showWhichPlayer()
{
    if( current_player == &player_arr[ 0 ] )
    {
        cout << endl << "Trwa tura pierwszego gracza " << endl;
    }
    else
    {
        cout << endl << "Trwa tura drugiego gracza " << endl;
    }
}

void Game::initiateShips()
{
    for( int i = 0; i < current_player->getShipCount(); i++ )
    {
        for( int x = 0; x < current_player->getShipSize( i ); x++)
        {
            player_board->insertShipsPositions( current_player->getShipY( i, x ), current_player->getShipX( i, x ) );
        }
    }
}

void Game::insert_hits()
{
    for( int i = 0; i < current_player->get_player_damage(); i++)
    {
        player_board->insert_shots_on_target( current_player->get_hits_takenY( i ), current_player->get_hits_takenX( i ));
    }

    for( int i = 0; i < opposing_player->get_player_damage(); i++)
    {
        shooting_board->insert_shots_on_target( opposing_player->get_hits_takenY( i ), opposing_player->get_hits_takenX( i ));
    }
}

void Game::insert_missfires()
{
    for( int i = 0; i < current_player->get_missfires_taken_counter(); i++)
    {
        player_board->insert_misses( current_player->get_missfires_takenY( i ), current_player->get_missfires_takenX( i ) );
    }

    for( int i = 0; i < opposing_player->get_missfires_taken_counter(); i++)
    {
        shooting_board->insert_misses( opposing_player->get_missfires_takenY( i ), opposing_player->get_missfires_takenX( i ) );
    }
}

void Game::gameInterface()
{
    if( players_ready )
    {
        cout << "Faza rozgrywania. Oznacz pozycje, ktora chcesz ostrzelac" << endl;
    }
    else
    {
        cout << endl << "Faza przygotowawcza: Oznacz statki na mapie" << endl;
        current_player->showplayerInfo1();
    }
    cout << endl << "Aby poruszyc sie po mapie, wcisnij odpowiedni klawisz i zatwierdz enter: " << endl;
    cout << "w - do gory" << endl;
    cout << "s - w dol" << endl;
    cout << "a - w lewo" << endl;
    cout << "d - w prawo" << endl;
    cout << "f - aby oznaczyc pozycje statku / strzelic " << endl;
    cout << "Nacisnij 0 aby wyjsc" << endl;

    cin >> action;

    switch( action )
    {

    case 'w':
        break;

    case 's':
        break;

    case 'a':
        break;

    case 'd':
        break;

    case 'f':
        if( players_ready )
        {
            Player_shoots();
            if( opposing_player->check_if_dead() )
            {
                gameOn = false;
                shoot_made = true;
            }
            else
            {
                shoot_made = true;
                switchPlayer();
            }
        }
        break;

    case 'c':
        break;

    case '0':
        gameOn = false;
        break;

    default:
        cout << endl << "Podano zla wartosc" << endl;
        cout << " gameInterface returned with incorect value" << endl;
        system( "pause" );
        break;
    }
}

////////////////////////////////////////////////////////

char Game::getAction()
{
    return action;
}

void Game::gamePlay()
{
    while( gameOn == true && players_ready == false )
    {
        player_board->clearBoard();
        initiateShips();
        player_board->insertPlayerPosition( current_player->getPositionY(), current_player->getPositionX() );
        battleship_display();
        player_board->displayBoard();
        player_board->clearPlayerPosition( current_player->getPositionY(), current_player->getPositionX() );
        showWhichPlayer();
        gameInterface();
        current_player->playerMove( getAction() );
        if( current_player->finished_setting_ships() )
        {
            if( check_if_ready() )
            {
                 players_ready = check_if_ready();
                 cout << "Koniec fazy przygotowan. Zaczynamy rozgrywke. " << endl;
                 switchPlayer();
                 system("pause");
            }
            else
            {
                switchPlayer();
            }
        }
        system( "cls" );
    }
}

bool Game::check_if_ready()
{
    if( player_arr[ 0 ].finished_setting_ships()==true && player_arr[ 1 ].finished_setting_ships()==true )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Game::gamePlayPhaseTwo()
{
    while( gameOn )
    {
        player_board->clearBoard();
        shooting_board->clearBoard();
        initiateShips();
        insert_missfires();
        insert_hits();
        shooting_board->insertPlayerPosition( current_player->getPositionY(), current_player->getPositionX() );
        battleship_display();
        cout << endl << " Plansza z Twoimi statkami " << endl;
        player_board->displayBoard();
        cout << endl << " Plansza do strzelania " << endl;
        shooting_board->displayBoard();
        shooting_board->clearPlayerPosition( current_player->getPositionY(), current_player->getPositionX() );
        showWhichPlayer();
        gameInterface();
        if(shoot_made == false)
        {
            current_player->playerMove( getAction() );
        }
        system( "cls" );
        shoot_made = false;
    }
}

void Game::Player_shoots()
{
    if( opposing_player->take_hit( current_player->getPositionY(), current_player->getPositionX() ) )
    {
        cout << endl << "Udalo sie trafic w statek przeciwnika!! " << endl;
        system( "pause" );
    }
    else
    {
        cout << endl << "Pudlo! Sprobuj jeszcze raz. " << endl;
        system( "pause" );
    }
    shoot_made = true;
}

